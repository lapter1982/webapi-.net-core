﻿using Newtonsoft.Json;
using System;

namespace StoreManage.Models
{
    public class CategoryModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("categoryCode")]
        public int? CategoryCode { get; set; }
    }
}

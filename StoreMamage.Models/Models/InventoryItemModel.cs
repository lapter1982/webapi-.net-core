﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Models
{
    public class InventoryItemModel
    {
        public ItemModel Item { get; set; }
        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
        public DateTime? UpdateDate { get; set; }
    }
}

﻿using Newtonsoft.Json;
using System;

namespace StoreManage.Models
{
    public class ItemModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("sku")]
        public string Sku { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("price")]
        public decimal? Price { get; set; }
        [JsonProperty("category")]
        public CategoryModel Category { get; set; }
    }
}

﻿using Newtonsoft.Json;

namespace StoreManage.Models
{
    public class MonthlyStatsRecord
    {
        [JsonProperty("dateOccured")]
        public string DateOccured { get; set; }
        [JsonProperty("transactionType")]
        public string Transaction { get; set; }
        [JsonProperty("amount")]
        public decimal? Amount { get; set; }
    }
}

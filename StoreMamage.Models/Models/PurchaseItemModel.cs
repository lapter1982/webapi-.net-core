﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Models
{
    public class PurchaseItemModel
    {
        [JsonProperty("sku")]
        public string Sku { get; set; } 
        [JsonProperty("quantity")]
        public int? Quantity { get; set; }
      
    }
}

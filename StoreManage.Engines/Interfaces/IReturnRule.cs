﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Engines.Interfaces
{
    public interface IRule<T>
    {
        int Priority { get; }
        string  Message { get; }
        T Execute(int Category, DateTime receiptDate); 
    }
}

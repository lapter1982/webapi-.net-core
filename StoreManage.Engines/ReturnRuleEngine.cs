﻿using StoreManage.Engines.Interfaces;
using StoreManage.Engines.Rules;
using StoreManage.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
namespace StoreManage.Engines
{
    public class ReturnRuleEngine
    {
        List<IRule<ReturnRuleResult>> Rules;
        Item item;
        DateTime receiptDate;
        public ReturnRuleEngine()
        {
            Rules = new List<IRule<ReturnRuleResult>>();
            Rules.Add(new ReturnRuleCash());
            Rules.Add(new ReturnRuleAfterLongTime());
            Rules.Add(new ReturnRuleUnderwear());
            Rules.Add(new ReturnRuleCheck());
        }


        public ReturnRuleResult ExecuteRules(int category, DateTime receiptDate)
        {
            ReturnRuleResult result = null;
            foreach (var rule in Rules.OrderBy(m => m.Priority))
            {
                result = rule.Execute(category, receiptDate);
                if (result != null)
                    break;
            }
            return result;
        }
    }
}

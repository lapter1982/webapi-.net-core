﻿using StoreManage.Shared;
using System;

namespace StoreManage.Engines
{
    public class ReturnRuleResult
    {
        public string Message { get; set; }
        public string ReturnMethod { get; set; }
    }
}

﻿using StoreManage.Engines.Interfaces;
using StoreManage.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Engines.Rules
{
    internal class ReturnRuleAfterLongTime : IRule<ReturnRuleResult>
    {

        const int ThirtyDays = 30;
        public int Priority { get { return 4; } }

        public string Message => "Nothing will be given to customer as exceeded 30 days";

        public ReturnRuleResult Execute(int category, DateTime receiptDate)
        {
            if (DateTime.Now.Subtract(receiptDate).Days > ThirtyDays)
            {
                return new ReturnRuleResult { Message = Message, ReturnMethod = Shared.ReturnMethod.Nothing.ToString() };
            }
            return null;
        }

    }
}

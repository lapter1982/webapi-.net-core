﻿using StoreManage.Engines.Interfaces;
using StoreManage.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Engines.Rules
{
    internal class ReturnRuleCash : IRule<ReturnRuleResult>
    {

        const int FiftenDays = 15;
        public int Priority { get { return 2; } }       
        public string Message => "Cash will be given to customer as in range of 15 days";

        public ReturnRuleResult Execute(int category, DateTime receiptDate)
        {
            if (DateTime.Now.Subtract(receiptDate).Days < FiftenDays)
            {
                return new ReturnRuleResult { Message = Message, ReturnMethod = Shared.ReturnMethod.Cash.ToString() };
            }
            return null;
        }

    }
}

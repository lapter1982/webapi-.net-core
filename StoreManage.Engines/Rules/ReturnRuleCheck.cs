﻿using StoreManage.Engines.Interfaces;
using StoreManage.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Engines.Rules
{
    internal class ReturnRuleCheck : IRule<ReturnRuleResult>
    {

        const int FiftenDays = 15;
        const int ThirtyDays = 30;
        public int Priority { get { return 3; } }
        public string Message => "Check will be given to customer as it is in range of 15 - 30 days";

        public ReturnRuleResult Execute(int category, DateTime receiptDate)
        {
            int daysFromRecepit = DateTime.Now.Subtract(receiptDate).Days;
            if (FiftenDays <= daysFromRecepit && daysFromRecepit <= ThirtyDays)
            {             
                return new ReturnRuleResult { Message = Message, ReturnMethod = Shared.ReturnMethod.Check.ToString() };
            }
            return null;
        }

    }
}

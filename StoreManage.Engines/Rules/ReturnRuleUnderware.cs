﻿using StoreManage.Engines.Interfaces;
using StoreManage.Persistance.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Engines.Rules
{
    internal class ReturnRuleUnderwear : IRule<ReturnRuleResult>
    {
        const int UnderwearCategory = 1;
   
        public int Priority { get { return 1; } }  

        public string Message => "Underware cannot be returned";

        public ReturnRuleResult Execute(int category, DateTime receiptDate)
        {
            if (category == UnderwearCategory)
            {            
                return new ReturnRuleResult { Message = Message,ReturnMethod = Shared.ReturnMethod.Nothing.ToString() };
            }
            return null;
        }

    }
}

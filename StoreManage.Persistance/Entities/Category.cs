﻿using System;
using System.Collections.Generic;

namespace StoreManage.Persistance.Entities
{
    public partial class Category
    {
        public Category()
        {
            Item = new HashSet<Item>();
        }

        public int Id { get; set; }
        public int? CategoryCode { get; set; }
        public string Name { get; set; }
        public DateTime? InsertDate { get; set; }

        public ICollection<Item> Item { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace StoreManage.Persistance.Entities
{
    public partial class InventoryItem
    {
        public int Id { get; set; }
        public int? ItemId { get; set; }
        public int? Quantity { get; set; }
        public DateTime? InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public Item Item { get; set; }
    }
}

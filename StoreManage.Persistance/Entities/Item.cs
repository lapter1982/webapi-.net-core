﻿using System;
using System.Collections.Generic;

namespace StoreManage.Persistance.Entities
{
    public partial class Item
    {
        public Item()
        {
            InventoryItem = new HashSet<InventoryItem>();
            Purchase = new HashSet<Purchase>();
            Return = new HashSet<Return>();
        }

        public int Id { get; set; }
        public string Sku { get; set; }
        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public decimal? Price { get; set; }
        public DateTime? InsertDate { get; set; }
        public DateTime? UpdateDate { get; set; }

        public Category Category { get; set; }
        public Item IdNavigation { get; set; }
        public Item InverseIdNavigation { get; set; }
        public ICollection<InventoryItem> InventoryItem { get; set; }
        public ICollection<Purchase> Purchase { get; set; }
        public ICollection<Return> Return { get; set; }
    }
}

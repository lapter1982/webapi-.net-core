﻿using System;
using System.Collections.Generic;

namespace StoreManage.Persistance.Entities
{
    public partial class Purchase
    {
        public int Id { get; set; }
        public int? ReceiptId { get; set; }
        public int? WorkerId { get; set; }
        public int? ItemId { get; set; }
        public decimal? Price { get; set; }
        public int? Quantity { get; set; }
        public DateTime? InsertDate { get; set; }

        public Item Item { get; set; }
        public Receipt Receipt { get; set; }
        public Worker Worker { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace StoreManage.Persistance.Entities
{
    public partial class Receipt
    {
        public Receipt()
        {
            Purchase = new HashSet<Purchase>();
            Return = new HashSet<Return>();
        }

        public int Id { get; set; }
        public int ReceiptNumber { get; set; }
        public DateTime? InsertDate { get; set; }

        public ICollection<Purchase> Purchase { get; set; }
        public ICollection<Return> Return { get; set; }
    }
}

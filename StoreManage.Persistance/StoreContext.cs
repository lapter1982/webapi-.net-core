﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace StoreManage.Persistance.Entities
{
    public partial class StoreContext : DbContext
    {
        public StoreContext()
        {
        }

        public StoreContext(DbContextOptions<StoreContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<InventoryItem> InventoryItem { get; set; }
        public virtual DbSet<Item> Item { get; set; }
        public virtual DbSet<Purchase> Purchase { get; set; }
        public virtual DbSet<Receipt> Receipt { get; set; }
        public virtual DbSet<Return> Return { get; set; }
        public virtual DbSet<Worker> Worker { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name).HasMaxLength(250);
            });

            modelBuilder.Entity<InventoryItem>(entity =>
            {
                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.InventoryItem)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_Inventory_Item");
            });

            modelBuilder.Entity<Item>(entity =>
            {
                entity.HasIndex(e => e.Sku)
                    .HasName("IX_Item")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Name).HasMaxLength(250);

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(250);

                entity.Property(e => e.UpdateDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Item)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Item_Category");

                entity.HasOne(d => d.IdNavigation)
                    .WithOne(p => p.InverseIdNavigation)
                    .HasForeignKey<Item>(d => d.Id)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Item_Item");
            });

            modelBuilder.Entity<Purchase>(entity =>
            {
                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Purchase)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_Purchase_Item");

                entity.HasOne(d => d.Receipt)
                    .WithMany(p => p.Purchase)
                    .HasForeignKey(d => d.ReceiptId)
                    .HasConstraintName("FK_Purchase_Receipt");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.Purchase)
                    .HasForeignKey(d => d.WorkerId)
                    .HasConstraintName("FK_Purchase_Worker");
            });

            modelBuilder.Entity<Receipt>(entity =>
            {
                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");
            });

            modelBuilder.Entity<Return>(entity =>
            {
                entity.Property(e => e.InsertDate)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.Price).HasColumnType("money");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.Return)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK_Return_Item");

                entity.HasOne(d => d.Receipt)
                    .WithMany(p => p.Return)
                    .HasForeignKey(d => d.ReceiptId)
                    .HasConstraintName("FK_Return_Receipt");

                entity.HasOne(d => d.Worker)
                    .WithMany(p => p.Return)
                    .HasForeignKey(d => d.WorkerId)
                    .HasConstraintName("FK_Return_Worker");
            });

            modelBuilder.Entity<Worker>(entity =>
            {
                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");
            });
        }
    }
}

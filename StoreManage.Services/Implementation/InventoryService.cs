﻿using StoreManage.Models;
using StoreManage.Persistance.Entities;
using StoreManage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using StoreManage.Shared;

namespace StoreManage.Services.Implementation
{
    public class InventoryService : IInventoryService
    {
        enum BalanceAction { Increase, Decrease }
        readonly StoreContext _storeContext = null;
        const string MissingValue = "{0} is missing";
        const string QuantityNegative = "Insufficent inventory level for item {0}";
        const string CategoryCodeWrong = "Wrong Category Code";
        public InventoryService(StoreContext storeContext)
        {
            _storeContext = storeContext;
        }
        public void AddInventoryItems(IEnumerable<InventoryItemModel> inventoryItems)
        {
            var existedItems = _storeContext.InventoryItem.Where(m => inventoryItems.Any(t => m.Item.Sku == t.Item.Sku));
            foreach (var invItem in inventoryItems)
            {
                var existedItem = existedItems.SingleOrDefault(m => m.Item.Sku == invItem.Item.Sku);
                if (existedItem != null)
                {
                    existedItem.Quantity += invItem.Quantity;
                    existedItem.UpdateDate = DateTime.Now;
                }
                else
                {
                    var newItem = CreateNewItem(invItem.Item);
                    _storeContext.InventoryItem.Add(new InventoryItem
                    {
                        ItemId = newItem.Id,
                        Quantity = invItem.Quantity,
                        UpdateDate = DateTime.Now
                    });
                }
                _storeContext.SaveChanges();
            }
        }

        public void DecreaseBalance(IEnumerable<InventoryItemModel> inventoryItems)
        {
            BalanceChange(inventoryItems.ToList(), BalanceAction.Decrease);
        }

        public void IncreaseBalance(IEnumerable<InventoryItemModel> inventoryItems)
        {
            BalanceChange(inventoryItems.ToList(), BalanceAction.Increase);
        }

        private void BalanceChange(List<InventoryItemModel> inventoryItems, BalanceAction balanceAction)
        {
            
            inventoryItems.ForEach(m =>
            {
                m.Quantity =(balanceAction == BalanceAction.Decrease) 
                ? (Math.Abs(m.Quantity.Value) * -1 ): Math.Abs(m.Quantity.Value);
            });
            var existedItems = _storeContext.InventoryItem.Where(m => inventoryItems.Any(t => m.Item.Sku == t.Item.Sku)).Include(m => m.Item);
            foreach (var item in existedItems)
            {
                var quantityInput = inventoryItems.Where(m => m.Item.Sku == item.Item.Sku).Sum(t => t.Quantity);
                var newQuantity = item.Quantity.Value
                    + quantityInput;
                if (newQuantity < 0)
                    throw new Exception(string.Format(QuantityNegative, item.Item.Sku));
                item.Quantity = newQuantity;
                item.UpdateDate = DateTime.Now;
             
            }
           // _storeContext.SaveChanges();

        }

        public IEnumerable<MonthlyStatsRecord> GetStatsByMonth(int year, int month)
        {
            var monthlyRecords = new List<MonthlyStatsRecord>();
            var returns = _storeContext.Return.Where(m => m.InsertDate.Value.Month == month && m.InsertDate.Value.Year == year).
                Select(m => new { TransactionType = TransactionType.Return, m.InsertDate, Amount = m.Price * m.Quantity }).ToList();

            var purchases = _storeContext.Purchase.Where(m => m.InsertDate.Value.Month == month && m.InsertDate.Value.Year == year).
                Select(m => new { TransactionType = TransactionType.Purchase, m.InsertDate, Amount = m.Price * m.Quantity }).ToList();

            var concatAll = returns.Concat(purchases);

            var result = from r in concatAll
                   group r by new {r.TransactionType,r.InsertDate.Value.Day, r.InsertDate.Value.Month } into Aggregated
                     
                      select new
                      {
                          Amount = Aggregated.Sum(t => t.Amount),
                          TransactionType = Aggregated.Key.TransactionType.ToString(),
                          Date = $"{Aggregated.Key.Month.ToString()}/{Aggregated.Key.Day}"
                      };
            monthlyRecords.AddRange(result.Select(m => 
            new MonthlyStatsRecord() { Amount = m.Amount, DateOccured = m.Date, Transaction = m.TransactionType }));
            return monthlyRecords;
        }

        private Item CreateNewItem(ItemModel item)
        {
            if (string.IsNullOrEmpty(item.Sku))
                throw new Exception(string.Format(MissingValue, nameof(item.Sku)));
            if (!item.Price.HasValue)
                throw new Exception(string.Format(MissingValue, nameof(item.Price)));
            if (item.Category ==null || !item.Category.CategoryCode.HasValue)
                throw new Exception(string.Format(MissingValue, nameof(item.Category)));
            var category = _storeContext.Category.SingleOrDefault(m => m.CategoryCode == item.Category.CategoryCode);

            if (category == null)
                throw new Exception(CategoryCodeWrong);

            var newItem = new Item { CategoryId = category.Id, Sku = item.Sku, Name = item.Name, Price = item.Price, UpdateDate = DateTime.Now };
            _storeContext.Item.Add(newItem);

            _storeContext.SaveChanges();

            return newItem;

        }

    }
}

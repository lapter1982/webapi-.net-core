﻿using StoreManage.Models;
using StoreManage.Persistance.Entities;
using StoreManage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace StoreManage.Services.Implementation
{
    public class PurchaseService : IPurchaseService
    {
        readonly StoreContext _storeContext = null;
        readonly IInventoryService _inventoryService = null;
        readonly static object syncObject = new object();
        public PurchaseService(StoreContext storeContext, IInventoryService inventoryService)
        {
            _storeContext = storeContext;
            _inventoryService = inventoryService;
        }

        public void PurchaseItems(int workerNum, IEnumerable<PurchaseItemModel> purchaseItems)
        {
            var inventoryItems =
              purchaseItems.Select(m => new InventoryItemModel { Quantity = m.Quantity, Item = new ItemModel { Sku = m.Sku } });
            _inventoryService.DecreaseBalance(inventoryItems);
            var existedItems = _storeContext.InventoryItem.Where(m => purchaseItems.Any(t => m.Item.Sku == t.Sku));
            var worker = _storeContext.Worker.Single(m => m.WorkerNumber == workerNum);
            var receipt = CreateReceipt();
            foreach (var item in existedItems)
            {
                var quantityPurchase = purchaseItems.Where(m => m.Sku == item.Item.Sku).Sum(t => t.Quantity);              
                _storeContext.Purchase.Add(new Purchase
                {
                    Price = item.Item.Price,
                    ItemId = item.ItemId,
                    Quantity = quantityPurchase,
                    ReceiptId = receipt.Id,
                    WorkerId = worker.Id
                });

            }
            _storeContext.SaveChanges();

        }

        private Receipt CreateReceipt()
        {
            lock (syncObject)
            {
                var previousReceipt = _storeContext.Receipt.LastOrDefault();
                var newReceipt = new Receipt() { ReceiptNumber = (previousReceipt?.ReceiptNumber ?? 0) + 1 };
                _storeContext.Receipt.Add(newReceipt);
                _storeContext.SaveChanges();
                return newReceipt;
            }
        }


    }
}

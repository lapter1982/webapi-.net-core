﻿using StoreManage.Models;
using StoreManage.Persistance.Entities;
using StoreManage.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using StoreManage.Engines;

namespace StoreManage.Services.Implementation
{
    public class ReturnService : IReturnService
    {
       
        readonly StoreContext _storeContext = null;
        readonly IInventoryService _inventoryService = null;
        public ReturnService(StoreContext storeContext, IInventoryService inventoryService)
        {
            _storeContext = storeContext;
            _inventoryService = inventoryService;
          
        }

        public IEnumerable<ReturnRuleResult> ReturnPolicies(int receiptNum, IEnumerable<ReturnItemModel> returnItems)
        {
            var rulesMet = new List<ReturnRuleResult>();
            var existedItems = _storeContext.InventoryItem.
                Where(m => returnItems.Any(t => m.Item.Sku == t.Sku)).Include(m => m.Item).ThenInclude(t=>t.Category);
            var receipt = _storeContext.Receipt.Single(m => m.ReceiptNumber == receiptNum);
            foreach (var item in existedItems)
            {
                var returnRuleEngine = new ReturnRuleEngine();
                rulesMet.Add(returnRuleEngine.ExecuteRules(item.Item.Category.CategoryCode.Value, receipt.InsertDate.Value));
            }

            return rulesMet;
        }
        public void ReturnItems(int receiptNum, int workerNum, IEnumerable<ReturnItemModel> returnItems)
        {
            var inventoryItems =
                returnItems.Select(m => new InventoryItemModel { Quantity = m.Quantity, Item = new ItemModel { Sku = m.Sku } });
            _inventoryService.IncreaseBalance(inventoryItems);

            var existedItems = _storeContext.InventoryItem.Where(m => returnItems.Any(t => m.Item.Sku == t.Sku)).Include(m => m.Item);
            var receipt = _storeContext.Receipt.Single(m => m.ReceiptNumber == receiptNum);
            var worker = _storeContext.Worker.Single(m => m.WorkerNumber == workerNum);
            foreach (var item in existedItems)
            {
                var quantityReturn = returnItems.Where(m => m.Sku == item.Item.Sku).Sum(t=>t.Quantity);               
                _storeContext.Return.Add(new Return
                {
                    Price = item.Item.Price,
                    ReceiptId = receipt.Id,
                    ItemId = item.Item.Id,
                    WorkerId = worker.Id,
                    Quantity = quantityReturn
                });

            }
            _storeContext.SaveChanges();
        }
    }
}

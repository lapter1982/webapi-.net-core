﻿
using StoreManage.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Services.Interfaces
{
    public interface IInventoryService
    {
        void AddInventoryItems(IEnumerable<InventoryItemModel> inventoryItems);
        void DecreaseBalance(IEnumerable<InventoryItemModel> inventoryItems);
        void IncreaseBalance(IEnumerable<InventoryItemModel> inventoryItems);
        IEnumerable<MonthlyStatsRecord> GetStatsByMonth(int year, int month);
    }
}

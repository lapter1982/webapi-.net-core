﻿using StoreManage.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Services.Interfaces
{
    public interface IPurchaseService
    {
        void PurchaseItems(int workerNum, IEnumerable<PurchaseItemModel> purchaseItems);
    }
}

﻿using StoreManage.Engines;
using StoreManage.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace StoreManage.Services.Interfaces
{
    public interface IReturnService
    {
        void ReturnItems(int receiptNum, int workerId, IEnumerable<ReturnItemModel> returnItems);
        IEnumerable<ReturnRuleResult> ReturnPolicies(int receiptNum, IEnumerable<ReturnItemModel> returnItems);
    }
}

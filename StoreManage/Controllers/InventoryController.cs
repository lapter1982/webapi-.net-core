﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StoreManage.Models;

using StoreManage.Services.Interfaces;

namespace StoreManage.Controllers
{
    [Route("inventory/")]
    [ApiController]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryService _inventoryService;

        private readonly IReturnService _returnService;

        private readonly IPurchaseService _purchaseService;
        public InventoryController(IInventoryService inventoryService, IReturnService returnService, IPurchaseService purchaseService)
        {
            _inventoryService = inventoryService;
            _purchaseService = purchaseService;
            _returnService = returnService;
        }
        [HttpPost]
        [Route("Add")]
        public ActionResult AddInventoryItems(IEnumerable<InventoryItemModel> inventroryItems)
        {
            _inventoryService.AddInventoryItems(inventroryItems);
            return Ok();
        }

        [HttpPut]
        [Route("Return/checkPolicies/{checkPolicies}/receipt/{receiptNum}/worker/{workerNum}")]
        public ActionResult Return(bool checkPolicies, int receiptNum, int workerNum, IEnumerable<ReturnItemModel> returnItems)
        {
            if (checkPolicies)
                return Ok(_returnService.ReturnPolicies(receiptNum, returnItems));
            else
                _returnService.ReturnItems(receiptNum, workerNum, returnItems);
            return Ok();
        }

        [HttpPut]
        [Route("Purchase/worker/{workerNum}")]
        public ActionResult Purchase(int workerNum, IEnumerable<PurchaseItemModel> purchaseItems)
        {
            _purchaseService.PurchaseItems(workerNum, purchaseItems);         
            return Ok();
        }
        [HttpGet]
        [Route("GetStatsByMonth/year/{year}/month/{month}")]
        public ActionResult GetStatsByMonth(int year, int month)
        {
            var result = _inventoryService.GetStatsByMonth(year, month);           
            return Ok(result);
        }
    }
}
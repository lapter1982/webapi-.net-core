﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StoreManage.Extensions;
using StoreManage.Persistance.Entities;
using StoreManage.Services;
using StoreManage.Services.Implementation;
using StoreManage.Services.Interfaces;

namespace StoreManage
{
    public class Startup
    {

        public IHostingEnvironment HostingEnvironment { get; private set; }
        public IConfiguration Configuration { get; private set; }
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1);
            services.AddScoped<IInventoryService, InventoryService>();
            services.AddScoped<IReturnService, ReturnService>();
            services.AddScoped<IPurchaseService, PurchaseService>();
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<StoreContext>(options => 
            options.UseSqlServer(connectionString)) ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
            app.ConfigureCustomExceptionMiddleware();
            app.UseHttpsRedirection();
            app.UseMvc();
        }

      

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            this.HostingEnvironment = env;
            this.Configuration = configuration;
        }
    }
}
